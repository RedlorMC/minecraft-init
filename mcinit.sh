#!/bin/bash
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program or from the site that you downloaded it
# from; if not, write to the Free Software Foundation, Inc., 59 Temple
# Place, Suite 330, Boston, MA  02111-1307   USA

#####Set script defaults prior to addressing arguments that may change configuration
##Use Shared Memory (RAM Drive). Minumum space in /dev/shm should be more than size of server directory.
##When set to 0 server will be run in /tmp
##Toggled when the -r option is supplied to the script
useSHM=0

##Specify default java memory arguments to be used
javaXMX=2G
javaXMN=1G

##Set/reset variable to trip error messages
errPresented=0

##Server Process ID set to 0
PID=0

################################
#####User Defined Variables#####
##### Set for your system  #####

##User name of the non-root user that will be used to run the server
RUNAS=redlor

##Path to the parent folder of the server
MCPATH=/home/redolor/minecraft

##Path to location of backups
BACKUPPATH=$MCPATH/backups

##IP Address of the server
SERVERIP=192.168.0.5

##SHM Directory to use for process run directory
dirSHM="/dev/shm"
##Disk directory to use for process run directory
dirDISK="/tmp"

##Total number of processor cores available to your system.
##Half of these cores (rounded down) will be used for java garbage collection.
TotalCores=2

##Define HELP and USAGE function
mcinitHelp() {
  ##display Help information
  echo "------------------------------------- "
  echo "Usage: mcinit.sh -s SERVER [OPTIONS] -c COMMAND"
  echo "This script is used for starting, stopping and managing minecraft server (or other java based server) processes from the shell."
  echo "Server processes are started in a back-grounded screen session allowing users to disconnect from the terminal without the server terminating."
  echo "After connecting to a console please remember to use the GNU Screen keyboard shortcut ('CTRL-a' then 'd') to disconnect from the screen session."
  echo "If you do not do this your server process will not remain running in the background."
  echo " "
  echo " -h | -?            Displays this help information."
  echo " "
  echo " -s SERVER          Specifies the server you want to control."
  echo " -c COMMAND         Specifies the command the script should perform."
  echo "     start          Starts a stopped server."
  echo "     stop           Stops running server."
  echo "     restart        Stops and Starts a server."
  echo "     monitor        Starts a server using the VMX Java variables allowing it to be monitored by using JavaVMX utilities."
  echo "     console        Attaches to screen console for running server."
  echo "     status         Displays status of server."
  echo "     info           Displays extended information for server."
  echo "     disksync       Synchronizes server from shared memory to disk."
  echo "     backup         Performs a full backup of the server from disk."
  echo " "
  echo " -r                 Utilize ramdisk for running server. Size of /dev/shm MUST be large enough for server and world growth or you will encounter issues."
  echo " -S                 Specifies use of small java memory profile."
  echo " -M                 Specifies use of medium java memory profile."
  echo " -L                 Specifies use of large java memory profile."
  echo " -G                 Specifies use of gargantuan java memory profile."
  echo " "
  echo "Example command incantations"
  echo " "
  echo " mcinit.sh -s SERVER -c start"
  echo " Starts server in the SERVER directory using defaults."
  echo " "
  echo " mcinit.sh -r -M -s tfc -c start"
  echo " Starts server in the tfc directory using ramdisk and the medium memory profile."
  echo " "
  echo " mcinit.sh -s apexcraft -c console"
  echo " Connects to the server console screen session."
  echo " "
  echo " mcinit.sh -s vanilla -c stop"
  echo " Stops running vanilla server."
  echo " "
  echo "------------------------------------- "
  exit 1
}

while getopts hs:c:rSMLG OPTION
do
  case $OPTION in
    h)
      mcinitHelp
      ;;
    s)
      SERVER=$OPTARG
      ;;
    c)
      ACTION=$OPTARG
      ;;
    r)
      useSHM=1
      ;;
    S)
      javaXMX=1G
      javaXMN=512m:
      ;;
    M)
      javaXMX=2G
      javaXMN=1G
      ;;
    L)
      javaXMX=4G
      javaXMN=1536m
      ;;
    G)
      javaXMX=6G
      javaXMN=2G
      ;;
  esac
done

if [ -z "$SERVER" ] | [ -z "$ACTION" ]
  then
    mcinitHelp
fi

############################################
#####Variables that are best left alone#####
#####         (Advanced users)         #####

##File name of the server jar file
SERVERJAR=$SERVER-server.jar

##Path containing the server jar and world files
SERVERPATH=$MCPATH/$SERVER

##Number of CPU cores to devote to java garbage collection
##Older systems will require this value to be reduced
GCCORES=$(expr $TotalCores / 2)

##Name for screen sessions
SESSION=mcinit_$SERVER

######################################
#####Variables used by the script#####
#####       (Expert users)       #####

###Java heap tuning
##Survivor spaces:1G; Max Heap:2G; Perm Gen size:100m; ratio between survivor spaces:16
#JAVAHEAP="-Xmn2G -Xmx6G -XX:PermSize=256m -XX:SurvivorRatio=16"
JAVAHEAP="-Xmn"$javaXMN" -Xmx"$javaXMX" -XX:PermSize=256m -XX:SurvivorRatio=16"

###Java Garbage Collect tunig
#Prevent java from pausing during garbage collection;set garbage collection to parallel on defined number of cores;enable parallel scavenge collector (tuned for large heaps)
JAVAGC="-XX:+UseParNewGC -XX:+UseConcMarkSweepGC -XX:+ExplicitGCInvokesConcurrentAndUnloadsClasses -XX:+UseNUMA"

###Java VMX 
##Configure java to allow connection from Java VMX analyzer/visualizer
JAVAVMX="-Djava.rmi.server.hostname=$SERVERIP -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.port=7575 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false"

###Java Invocation
INVOCATION="java $JAVAHEAP $JAVAGC -jar $SERVERJAR nogui"
VMXINVOCATION="java $JAVAHEAP $JAVAGC $JAVAVMX -jar $SERVERJAR nogui"

#####Checking for proper environment before running script#####
##Prevent script from executing as root user
if [ $RUNAS == root ]; then
  echo "Sorry, running things as the root user is a bad idea. You will need to run this script as a non-root user or hand-edit the script to allow for execution as root."
  exit 1
fi
##Ensure that screen utility is available
which screen > /dev/null
if [ $? -ne 0 ]; then
  echo "You will need to install or make available the GNU screen utility before this script can be used."
  exit 1
fi

##Set run directory as needed based on SHM settings
if [ $useSHM -eq 1 ]
then
  runDirectory=$dirSHM
else
  runDirectory=$dirDISK
fi

#####Define functions to be used as normal operation of script#####
runAs() {
  #Function receives argument that is a command to execute
  runAsCMD=$1
  if [ $(whoami) == $RUNAS ]
  then
    ##if I am who I should be execute the command
    bash -c "$runAsCMD"
  else
    ##ensure that script is being run with root permissions, otherwise user switch will not happen successfully
    if [ "`id -u`" != 0 ]; then
      echo "Sorry. Script needs to switch user context to $RUNAS. The script needs to be executed as the root user or as $RUNAS."
      exit 1
    fi
   ##if I am not who I should be then switch user to execute commands
   su - $RUNAS -c "$runAsCMD"
  fi
}

ErrServerOff() {
  if [ $errPresented -ne 1 ]
  then
   ##notify and  error out
   echo "[WARNING] $SERVER was not running."
   errPresented=1
   return 1
  else
   return 1
  fi
}

minecraftCommand() {
  if serverStatus
  then
  ##function receives argument
  minecraftCMD=$1
  ##send argument to screen session as minecraft terminal command
  runAs "screen -p 0 -S $SESSION -X eval 'stuff \"$minecraftCMD\"\015'"  
  else
    ##notify and  error out
    ErrServerOff
  fi
}

getPID() {
 PID="$(ps ax|grep -i java|grep -iv screen|grep -i $SERVERJAR|awk '{print $1}')"
}

serverStatus() {
  ##Determine if there is an active process for the server
  if [ $PID -ne 0 ]
    then
      return 0
  elif ps -ef|grep -i java|grep -i $SERVERJAR > /dev/null
  then
    ##Get Process ID for the running server process
    getPID
    return 0
  ##if no active process 
  else
    return 1
  fi
}

serverStart() {
  ##if the server is running
  if serverStatus
  then
    ##notify and error out
    echo "[ERROR] $SERVER was already running (pid $PID)"
    exit 1
  ##if the server is not running
  else
    ##Create a directory in shared memory
    runAs "mkdir \"$runDirectory/$SERVER\""
    ##Copy/sync files to shared memory
    runAs "rsync -chLprtu  \"$SERVERPATH/\"* \"$runDirectory/$SERVER\""
    ##Change to shared memory dir and execute screen session that will run the server
    runAs "cd \"$runDirectory/$SERVER\" && screen -c /dev/null -dmS $SESSION $INVOCATION"
    sleep 5
    ##Check that server start occured
    ##if the server is running
    if serverStatus
    then
      ##notify and supply pid
      echo "$SERVER is now running (pid $PID)"
    ##if server is not running
    else
      #notify and error out
      echo "[ERROR] Could not start $SERVER"
      exit 1
    fi
  fi
}


serverStop() {
  ##if the server is running
  if serverStatus
  then
    ##send commands to screen session running minecraft
    ##send save-all command to minecraft
    minecraftCommand "save-all"
    sleep 2
    ##send stop command to minecraft
    minecraftCommand "stop"
    sleep 60
    if serverStatus
    then
      ##notify and error out
      echo "Server process stopped successfully."
      exit 1
    fi
  ##if server was not running
  else
    ##notify and  error out
    ErrServerOff
    exit 1
  fi

  ##check server after stop command
  ##if server is running
  PID=0
  if serverStatus
  then
    ##notify and  error out
    echo "[ERROR] $SERVER did not shutdown in time allotted."
    exit 1
  ##if the server is nto running
  else
    ##sync contents of shared memory back to disk
    diskSync
    ##remove the shared memory directory
    runAs "rm -rf \"$runDirectory/$SERVER\""
  fi
}

attachConsole() {
  ##if the server is running
  if serverStatus
  then
    ##reattach to the screen session (must use keystrokes 'CTRL-a' then 'd' to disconnect)
    runAs "screen -S $SESSION -dr"
  ##if server is not running
  else
    ##notify and error out
    ErrServerOff
    exit 1
  fi
}

diskSync() {
   ##if a shared memory directory for the server exists
  if [ -d $dirDISK/$SERVER ]
   then
    ##sync directory's contents back to the disk
    runAs "rsync -chLprtu \"$dirDISK/$SERVER/\"* \"$SERVERPATH\""
  fi
  ##if a shared memory directory for the server exists
  if [ -d $dirSHM/$SERVER ]
   then
    ##sync directory's contents back to the disk
    runAs "rsync -chLprtu \"$dirSHM/$SERVER/\"* \"$SERVERPATH\""
  fi
}

saveOn() {
  ##if the server is running
  if serverStatus
  then
    ##enable save and notify
    minecraftCommand "save-on"
  ##if server is not running
  else
    ##notify and error out
    ErrServerOff
  fi
}

saveOff() {
  ##if the server is running
  if serverStatus
  then
    ##notify users, disable saves and write save queue
    minecraftCommand "save-off"
    minecraftCommand "save-all"
    ##wait and flush file system buffers
    sleep 5
    sync
    sleep 5
  ##if server is not running
  else
    ##notify and error out
    ErrServerOff
  fi
}

serverInfo() {
  ##if the server is running
  if serverStatus
  then
    ##get process ID of screen session
    ScreenPID=$(ps ax|grep -i java|grep -i screen|grep -i $SERVERJAR|awk '{print $1}')
    ##get memory use of screen session
    ScreenMemUse=$(pmap -x $ScreenPID|tail -n 1|awk '{print$3}')
    ##get memory use of java process
    MemUse=$(pmap -x $PID|tail -n 1|awk '{print $3}')
    ##get level name from server.properties
    LEVELNAME="$(cat $SERVERPATH/server.properties | grep -E 'level-name' | cut -d '=' -f 2)"
    ##get port number fro server.properties
    SERVERPORT="$(cat $SERVERPATH/server.properties | grep -E 'server-port' | cut -d '=' -f 2)"
    ##output server information to screen
    echo " $SERVER"
    echo " +Screen Process ID	: $ScreenPID"
    echo " | +Screen Session	: $SESSION"
    echo " | +Memory Use		: $ScreenMemUse"
    echo " +Java Process ID	: $PID"
    echo " | +Java Path		: $(readlink -f $(which java))"
    echo " | +Java Arguments	: $(ps -ef|grep $PID|grep -v grep|tr -s ' '|cut -d ' ' -f 9-)"
    echo " | +Java Memory Use	: $[$MemUse/1024]Mb"
    echo " | +Process Time	: $(ps -p $PID -o etime=|sed 's/^ *//') [[dd-]hh:]mm:ss"
    echo " | +Process PWD         :" $(pwdx $PID)
    echo " +Server Path		: $SERVERPATH"
    echo " +Server Port		: $SERVERPORT"
    echo " +Level Name		: $LEVELNAME"
  ##if the server is not running
  else
    #notify and error out
    ErrServerOff
    exit 1
  fi
}



serverBackup() {
  ##if backup path does not exist
  if ! [ -d $BACKUPPATH ]; then
    ##Create the directory
    mkdir $BACKUPPATH
  fi
  ##sync server directory to backup directory
  runAs "rsync -chLprtu \"$SERVERPATH\"* \"$BACKUPPATH/$SERVERNAME\""
  sleep 30
  ##get date and time 
  NOW="$(date +%Y-%m-%d.%H-%M-%S)"
  ##create a compressed backup file but background the command so we can get back to enabling saves on the server
  runAs "cd "$BACKUPPATH"; tar cfz \""$SERVER"_backup_"$NOW".tar.gz\" \"$SERVER\";rm -rf \"$BACKUPPATH/$SERVER\" &"
  ##remove backups that are older than 7 days
  runAs "cd \"$BACKUPPATH\" && find . -name \"*backup*\" -type f -mtime +14|xargs rm -fv"
}

case $ACTION in
start)
  serverStart
  ;;
stop)
  serverStop
  echo "Stop command sent"
  ;;
restart)
  serverStop
  serverStart
  ;;
monitor)
  INVOCATION=$VMXINVOCATION
  serverStart
  echo "Start command sent"
  ;;
console)
  attachConsole
  ;;
status)
  if serverStatus; then
    echo "$SERVER is running as process ID: $PID"
  else
    echo "$SERVER is not running"
  fi
  ;;
info)
  serverInfo
  ;;
disksync)
  minecraftCommand "say Beginning disk syncronization."
  saveOff
  diskSync
  saveOn
  minecraftCommand "say Syncronization complete."
  ;;
backup)
  minecraftCommand "say Beginning server backup."
  saveOff
  diskSync
  serverBackup
  saveOn
  minecraftCommand "say Backup completed."
  ;;
*)
  mcinitHelp
  ;;
esac
