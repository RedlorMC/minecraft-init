<html>
<body>
<h1> Redlor's PHP front-end for mcinit.sh </h1>
<hr />
This front end is for controlling Redlor's Minecraft servers and is born from the need for Kittychanley to shutdown the TFC Server following our weekly recordings.
<hr />
<b>Server to control</b><br />
Choose the server below that you would like to control. This is a required selection.<br />
<form action="mcinit.php" method="post">
<?php
chdir('/home/willijt/minecraft-nfs');
exec("ls|grep -v backups|grep -v template|grep -v mcinit.sh", $output);
foreach($output as $key => $label): ?>
<input type="radio" name="SrvChoice" value="<?php echo $label; ?>"><?php echo $label; ?><br />
<?php endforeach;
?>
<p />
<b>Execute Command</b><br />
Choose the command you want to have run against the server. This is a required selection.<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="start") echo "checked";?> value="start">Start<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="stop") echo "checked";?> value="stop">Stop<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="restart") echo "checked";?> value="restart">Restart<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="status") echo "checked";?> value="status" checked="checked">Status<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="info") echo "checked";?> value="info">Info<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="disksync") echo "checked";?> value="disksync">DiscSync<br />
<input type="radio" name="ExeCommand" <?php if (isset($ExeCommand) && $ExeCommand=="backup") echo "checked";?> value="backup">Backup<br />
<p />
<b>RAM Disk Use</b><br />
Control the use of RAM Disk for the purpose of running this server. This selection is optional.<br />
<input type="radio" name="RAMDisk" <?php if (isset($RAMDisk) && $RAMDisk=="-r") echo "checked";?> value="-r">Use RAM Disk<br />
<input type="radio" name="RAMDisk" <?php if (isset($RAMDisk) && $RAMDisk=="") echo "checked";?> value="" style="display:none;" checked="checked">
<p />
<b>Memory Profile</b><br />
Choose the Java memory profile that you would like to have used for the server environment. This selection is only necessary when starting a server and it is 100% optional.<br />
<input type="radio" name="MemProfile" <?php if (isset($MemProfile) && $MemProfile=="-S") echo "checked";?> value="-S">Small (1G)<br />
<input type="radio" name="MemProfile" <?php if (isset($MemProfile) && $MemProfile=="-M") echo "checked";?> value="-M">Medium (2G)<br />
<input type="radio" name="MemProfile" <?php if (isset($MemProfile) && $MemProfile=="-L") echo "checked";?> value="-L">Large (4G)<br />
<input type="radio" name="MemProfile" <?php if (isset($MemProfile) && $MemProfile=="-G") echo "checked";?> value="-G">Gargantuan (6G)<br />
<p />
<input type="submit">
</form>
</body>
</html>
